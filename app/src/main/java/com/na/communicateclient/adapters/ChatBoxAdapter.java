package com.na.communicateclient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.na.communicateclient.R;
import com.na.communicateclient.models.Message;

import java.util.List;

public class ChatBoxAdapter extends RecyclerView.Adapter<ChatBoxAdapter.ViewHolder> {
    private List<Message> MessageList;


    public ChatBoxAdapter(List<Message> MessagesList) {

        this.MessageList = MessagesList;
        System.out.println("List is " + MessagesList);

    }

    @Override
    public int getItemCount() {
        return MessageList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);


        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        //binding the data from our ArrayList of object to the item.xml using the viewholder

        System.out.println("Binding " + position );
        System.out.println();
        Message m = MessageList.get(position);
        holder.nickname.setText(m.getNickname());

        holder.message.setText(m.getContent());


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nickname;
        public TextView message;


        public ViewHolder(View view) {
            super(view);

            nickname = view.findViewById(R.id.nickname);
            message = view.findViewById(R.id.content);


        }
    }


}