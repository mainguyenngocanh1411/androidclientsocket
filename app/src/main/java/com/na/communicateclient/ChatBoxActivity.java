package com.na.communicateclient;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.na.communicateclient.adapters.ChatBoxAdapter;
import com.na.communicateclient.models.Message;

import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

public class ChatBoxActivity extends AppCompatActivity {

    private String nickName;

    private Button send;
    Client myClient;
    EditText message;
    String ip;
    public RecyclerView myRecylerView ;
    public List<Message> MessageList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_box);

        //Declare
        send = findViewById(R.id.send);
        message = findViewById(R.id.content);
        MessageList = new ArrayList<>();
        myRecylerView = findViewById(R.id.messagelist);
        nickName = Objects.requireNonNull(getIntent().getExtras()).getString(MainActivity.NICKNAME);
        ip = Objects.requireNonNull(getIntent().getExtras()).getString(MainActivity.IPADDRESS);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());

        //Create socket
        myClient = new Client(ip, 8080, nickName, this);
        myClient.execute();

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Send to server--------- ");
                SendMessage sendMessage = new SendMessage();
                sendMessage.start();




            }
        });


    }
    public String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress
                            .nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "Server running at : "
                                + inetAddress.getHostAddress();
                    }
                }
            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }
        return ip;
    }
    class SendMessage extends Thread {
        @Override
        public void run() {
            String content = message.getText().toString();
            if(!content.equals("")){
                OutputStream outputStream;
                outputStream = myClient.outputStream;
                PrintStream printStream = new PrintStream(outputStream);
                printStream.print(nickName+"-message:"+ content +"\n");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        message.setText("");
                    }
                });

            }


        }

    }


}
