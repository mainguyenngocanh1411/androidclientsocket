package com.na.communicateclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button startButton;
    private EditText nickname;
    private EditText ip;
    public static String ipAddress;
    public static final String NICKNAME = "usernickname";
    public static final String IPADDRESS = "ipaddress";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton = findViewById(R.id.enterchat);
        nickname = findViewById(R.id.nickname);
        ip = findViewById(R.id.ip);

        startButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Check if nickname is empty or not
                if (!nickname.getText().toString().isEmpty()) {

                    Intent i = new Intent(MainActivity.this, ChatBoxActivity.class);
                    System.out.println("IP in main " + ip.getText().toString());
                    //retreive nickname from EditText and add it to intent extra
                    i.putExtra(NICKNAME, nickname.getText().toString());
                    i.putExtra(IPADDRESS,ip.getText().toString());
                    startActivity(i);
                }
            }

        });
    }
    
}
