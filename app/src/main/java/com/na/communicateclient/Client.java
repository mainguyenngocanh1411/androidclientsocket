package com.na.communicateclient;

import android.os.AsyncTask;
import android.widget.Toast;

import com.na.communicateclient.adapters.ChatBoxAdapter;
import com.na.communicateclient.models.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Client extends AsyncTask<Void, String, Void> {

    String dstAddress;
    int dstPort;
    String response = "";
    String nickname = "";
    ChatBoxActivity activity;
    Socket socket;
    InputStream inputStream;
    OutputStream outputStream;
    List<Message> listMessage = new ArrayList<>();

    Client(String addr, int port, String nickname, ChatBoxActivity activity) {
        dstAddress = addr;
        dstPort = port;
        this.activity = activity;
        this.nickname = nickname;

    }

    public List<Message> getListMessage(){
        return this.listMessage;
    }

    @Override
    protected Void doInBackground(Void... arg0) {


        socket = null;
        inputStream = null;
        outputStream = null;
        try {
            socket = new Socket(dstAddress, dstPort);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                    1024);
            byte[] buffer = new byte[1024];
            int bytesRead;
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();

            //Send nickname to server
            PrintStream printStream = new PrintStream(outputStream);
            printStream.print("NN:"+nickname+"\n");

            //Read data from server
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
                response = byteArrayOutputStream.toString("UTF-8");
                //Split message
                final String[] parts = response.split("\n");
                final int length = parts.length;

                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        String lastestMessage = parts[length-1];

                        //Check if new user joins the room
                        if(lastestMessage.split(":")[0].equals("NN")){
                            final Toast toast = Toast.makeText(activity.getApplicationContext(),lastestMessage.split(":")[1], Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        else{
                            //TODO with normal message
                            System.out.println("Response " + lastestMessage);
                            String[] data = lastestMessage.split(":");
                            String nickname = data[0].split("-")[0];
                            String content = data[1];
                            listMessage.add(new Message(nickname+": ",content));
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println(listMessage);
                                    ChatBoxAdapter chatBoxAdapter = new ChatBoxAdapter(listMessage);
                                    chatBoxAdapter.notifyDataSetChanged();
                                    activity.myRecylerView.setAdapter(chatBoxAdapter);
                                    System.out.println(chatBoxAdapter);
                                    activity.myRecylerView.refreshDrawableState();

                                }
                            });

                        }

                    }
                });


            }


        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "IOException: " + e.toString();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        super.onPostExecute(result);
    }

}